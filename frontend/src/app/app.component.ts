import {Component, OnInit} from '@angular/core';

import {MenuController, Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {AuthService} from './shared/services/auth/auth.service';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
    public selectedIndex = 0;
    public isAdmin = false;
    public appPages = [
        {
            title: 'Home',
            url: '/dashboard'
        },
        {
            title: 'My Offers',
            url: '/bids',
        },
        // {
        //     title: 'Meine Lose',
        //     url: '/current-ticket',
        // },
        {
            title: 'My Wishlist',
            url: '/favourite',
        },
        // {
        //     title: 'Lose kaufen',
        //     url: '/buying-lose'
        // },
        {
            title: 'Help',
            url: '/hlife',
        },
        {
            title: 'Imprint',
            url: '/imprint',
        },
        {
            title: 'Data Privacy',
            url: '/privacy',
        },
        {
            title: 'Edit Profile',
            url: '/profile'
        },
        {
            title: 'Logout',
            url: '/'
        }
    ];

    currentUser;

    public additionalMenu = [
        {
            title: 'Create Product',
            url: '/admin/create_item'
        },
        {
            title: 'View Product',
            url: '/admin/view_item'
        }
    ];

    public unLoggedMenu = [
        {
            title: 'Home',
            url: '/home'
        },
        {
            title: 'Help',
            url: '/hlife',
        },
        {
            title: 'Imprint',
            url: '/imprint',
        },
        {
            title: 'Data Privacy',
            url: '/privacy',
        },
    ];

    constructor(
        private platform: Platform,
        private menu: MenuController,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        public authService: AuthService
    ) {
        this.initializeApp();
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
        });
    }

    ngOnInit() {
        const path = window.location.pathname.split('folder/')[1];
        if (path !== undefined) {
            this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
        }

        this.authService.currentUserSubject.subscribe(value => {
            this.isAdmin = value ? value.isAdmin : false;
            this.currentUser = value;
            if (this.isAdmin) {
                this.appPages = this.additionalMenu.concat(this.appPages);
            } else {
                if (this.appPages.length === 12) {
                    this.appPages.splice(0, 2);
                }
            }
        });
    }

    logout() {
        this.authService.logout();
        this.menu.close('menu');
    }

    onSelect(i: number) {
        this.selectedIndex = i;
        if (this.isAdmin && this.selectedIndex === 9) {
            this.logout();
        }
        if (!this.isAdmin && this.selectedIndex === 7) {
            this.logout();
        }
    }
}
