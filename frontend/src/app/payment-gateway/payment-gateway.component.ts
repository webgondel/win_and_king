import { Component, ElementRef, Input, OnInit, Output,EventEmitter , ViewChild } from '@angular/core';
import { Platform } from '@ionic/angular';
import { AuthService } from '../shared/services/auth/auth.service';
import PaymentGatewayService from './payment-gateway.service';

declare var paypal;

@Component({
  selector: 'app-payment-gateway',
  templateUrl: './payment-gateway.component.html',
  styleUrls: ['./payment-gateway.component.scss'],
})
export class PaymentGatewayComponent implements OnInit {
  @ViewChild('paypal', { static: true }) paypalElement: ElementRef;
  @Output() onSuccess = new EventEmitter();
  @Input() itemId: string;
  @Input() price: number;
  @Input() quantity: number;

  constructor(
    public authService: AuthService,
    public platform: Platform,
    public paymentGatewayService: PaymentGatewayService,
  ) { }

  ngOnInit() {
    paypal
      .Buttons({
        style: {
          tagline: false,
          fundingicons: 'true',
        },
        createOrder: (data, actions) => {
          return actions.order.create({
            purchase_units: [
              {
                description: 'Ticket!',
                amount: {
                  currency_code: 'EUR',
                  value: this.price * this.quantity
                }
              }
            ]
          });
        },
        onApprove: async (data, actions) => {
          this.paymentGatewayService.payTicket(data.orderID, this.itemId, this.quantity, (item) => {
            this.onSuccess.emit(item);
          })
        },
        onError: err => {
          console.log(err);
        }
      })
      .render(this.paypalElement.nativeElement);
  }
}
