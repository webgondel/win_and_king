import { Injectable } from '@angular/core';
import { LoadingController, ToastController } from '@ionic/angular';
import { HttpService } from '../shared/services/http/http.service';



// SUPPORTS only paypal for now
@Injectable({
    providedIn: 'root'
})
class PaymentGatewayService {
    constructor(
        private http: HttpService,
        public loadingController: LoadingController,
        public toastController: ToastController
    ) { }
    // Pay Ticket
    async payTicket(orderId: string, itemId: string, quantity: number, successCallback) {
        const loading = await this.loadingController.create({ message: 'Please wait...' });
        await loading.present();
        let toast;
        try {
            const { content, item } = await this.http.payTicket({ orderId, itemId, quantity })
            toast = await this.toastController.create({
                message: content,
                duration: 2000,
                color: "success"
            });
            successCallback(item)
        } catch (e) {
            console.log(e);
            toast = await this.toastController.create({
                message: "Something went wrong",
                duration: 2000,
                color: "danger"
            });
        }
        loading.dismiss();
        toast.present();
    }
}

export default PaymentGatewayService