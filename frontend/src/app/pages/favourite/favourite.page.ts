import {Component, OnInit} from '@angular/core';
import {LoadingController, MenuController} from '@ionic/angular';
import {Router} from '@angular/router';

import {HttpService} from '../../shared/services/http/http.service';
import {AuthService} from '../../shared/services/auth/auth.service';
import {environment} from '../../../environments/environment';


@Component({
    selector: 'app-favourite',
    templateUrl: './favourite.page.html',
    styleUrls: ['./favourite.page.scss'],
})
export class FavouritePage implements OnInit {

    public currentUser: any;
    public allFavourites: any[];

    public BASE_URL = environment.BASE_URL;

    constructor(
        public menu: MenuController,
        public httpRequest: HttpService,
        public authService: AuthService,
        public loadingController: LoadingController,
        public router: Router
    ) {
    }

    ngOnInit() {
    }

    async ionViewWillEnter() {
        this.currentUser = this.authService.currentUserValue;
        const loader = await this.loadingController.create();
        loader.present();

        this.httpRequest.getAllFavourite(this.currentUser.id).subscribe(res => {
            this.allFavourites = res;
            loader.dismiss();
        });
    }

    openMenu() {
        this.menu.enable(true, 'menu');
        this.menu.open('menu');
    }

    getImageFileName(item) {
        return JSON.parse(item.preview_images)[0];
    }

    formatDate(d) {
        const date = new Date(d);
        let dd = date.getDate().toString();
        let mm = (date.getMonth() + 1).toString();
        const yyyy = date.getFullYear();
        if (date.getDate() < 10) {
            dd = '0' + dd;
        }
        if (date.getMonth() < 9) {
            mm = '0' + mm;
        }
        return d = dd + '.' + mm + '.' + yyyy;
    }

    gotoProduct(favourite: any) {
        this.router.navigate(['/dashboard/item/' + favourite.item.item_id]);
    }
}
