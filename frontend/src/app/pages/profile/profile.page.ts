import {Component, OnInit} from '@angular/core';
import {MenuController, ToastController} from '@ionic/angular';
import {AuthService} from '../../shared/services/auth/auth.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MustMatch} from '../../shared/helper/mustmatch';
import {first} from "rxjs/operators";

@Component({
    selector: 'app-profile',
    templateUrl: './profile.page.html',
    styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

    public currentUser: any;
    public userInfoForm: FormGroup;
    public passwordResetForm: FormGroup;
    public error: any = null;
    public errorPassword: any = null;

    constructor(
        private menu: MenuController,
        public toastController: ToastController,
        public authService: AuthService,
        public formBuilder: FormBuilder,
    ) {
    }

    ngOnInit() {
        this.currentUser = this.authService.currentUserValue;
        this.userInfoForm = this.formBuilder.group({
            email: [this.currentUser.email, Validators.compose([
                Validators.required, Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-z]{2,4}$')
            ])],
            username: [this.currentUser.username, Validators.compose([
                Validators.required
            ])],
        });

        this.passwordResetForm = this.formBuilder.group({
            oldPassword: [null, Validators.compose([
                Validators.required
            ])],
            newPassword: [null, Validators.compose([
                Validators.required
            ])],
            confirmPassword: [null, Validators.compose([
                Validators.required
            ])],
        }, {
            validator: MustMatch('newPassword', 'confirmPassword')
        });
    }

    get f() {
        return this.userInfoForm.controls;
    }

    get p_f() {
        return this.passwordResetForm.controls;
    }

    openMenu() {
        this.menu.enable(true, 'menu');
        this.menu.open('menu');
    }

    save() {
        if (!this.userInfoForm.valid) {
            return;
        }

        const user = {
            id: this.currentUser.id,
            email: this.f.email.value.toLowerCase(),
            username: this.f.username.value
        };

        this.authService.updateUserInfo(user).pipe(first()).subscribe(
            res => {
                this.currentUser = this.authService.currentUserValue;
                this.error = null;
                this.presentToast('Updated successfully.');
            },
            error => {
                this.error = error.error.message;
                this.presentToast('Failed');
            }
        );
    }

    resetPassword() {
        if (!this.passwordResetForm.valid) {
            return;
        }

        const password = {
            id: this.currentUser.id,
            oldPassword: this.p_f.oldPassword.value,
            newPassword: this.p_f.newPassword.value
        };

        this.authService.updatePassword(password).pipe(first()).subscribe(
            res => {
                if (res.message === 'success') {
                    this.passwordResetForm.reset();
                    this.errorPassword = null;
                    this.presentToast('Update successfully.');
                } else {
                    this.presentToast('Password is not correct.');
                }
            },
            error => {
                this.passwordResetForm.reset();
                this.errorPassword = error.error.message;
                this.presentToast('Password is not correct.');
            }
        );
    }

    async presentToast(msg) {
        const toast = await this.toastController.create({
            message: msg,
            duration: 2000
        });
        toast.present();
    }
}
