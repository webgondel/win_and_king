import {Component, OnInit} from '@angular/core';
import {MenuController} from '@ionic/angular';
import {Router} from '@angular/router';
import {AuthService} from '../../shared/services/auth/auth.service';


@Component({
    selector: 'app-hlife',
    templateUrl: './hlife.page.html',
    styleUrls: ['./hlife.page.scss'],
})
export class HlifePage implements OnInit {

    currentUser: any;
    public items: any = [];

    constructor(
        private menu: MenuController,
        public authService: AuthService,
        public router: Router,
    ) {
        this.items = [
            {
                expanded: false,
                title: 'Wo kann ich mir die Produkte real Ansehen',
                content: 'Wir arbeiten mit verschiedenen Autohäusern, Immobilienmakler und\n' +
                    'Hotels/Reiseveranstalter auf der ganzen Welt zusammen. Sie können die\n' +
                    'einzelnen Standorte unserer Produkte links oben auf den Infotaste\n' +
                    'ansehen. Selbstverständlich können Sie sich in mit unseren Partnern in\n' +
                    'Kontakt setzen und persönlich die Produkte anschauen.',
                link: null
            },
            {
                expanded: false,
                title: 'Zeitgrenze des Gebots',
                content: 'Die Produkte werden ohne Zeitlimit angeboten. Die Auktion wird beendet,\n' +
                    'sobald der Gesamtlos, den man über die Produkte sehen kann, erreicht\n' +
                    'worden ist. Wenn jedoch eine Produkt nach langer Zeit immer noch nicht\n' +
                    'das Gesamtlos erreicht hat, wird diese Produkt entfernt indem Sie vorher\n' +
                    'informiert werden und Sie die Möglichkeit haben die gebotenen Lose in\n' +
                    'einen anderen Produkt an zu bieten.\n',
                link: null
            },
            {
                expanded: false,
                title: 'Auslosung: Wann, Wie, Wo',
                content: 'Wann…\n' +
                    '…die Auslosungen finden statt sobald der Gesamtlos des Produktes\n' +
                    'erreicht worden ist. Sie werden sofort benachrichtigt und über die\n' +
                    'Auslosung informiert.\n' +
                    'Wie…\n' +
                    '…die Auslosungen werden immer gemeinsam mit einem Offiziellen Notar\n' +
                    'getätigt. Dementsprechend wie viele Lose einzelne Nutzer für diesen\n' +
                    'Produkt geboten haben, werden alle Benutzernamen in einem Topf\n' +
                    'hinzugefügt. Je wertvoller das Produkt ist je mehr Namen werden vom\n' +
                    'Topf herausgezogen. Alle Nutzer die herausgezogen worden sind haben\n' +
                    'schonmal hunderte von Lose gewonnen und werden danach in einen\n' +
                    'weiteren Topf hinzugefügt um danach nur noch einen Namen\n' +
                    'herauszuziehen der dann auch der Gewinner des Produktes sein wird.\n' +
                    'Wo…\n' +
                    '…die Standorte sind je Produkt immer unterschiedlich. Die Verlosungen\n' +
                    'werden Live auf unseren YouTube Chanel übertragen.',
                link: null
            },
            {
                expanded: false,
                title: 'Wenn ich gewonnen habe',
                content: 'Sobald Sie als Sieger ausgelost worden sind, werden wir Ihnen eine E-Mail\n' +
                    'zusenden mit einen Kontaktformular was Sie ausgefüllt zu uns zurück\n' +
                    'Schicken werden. Nachdem wir Ihr Formular bearbeitet haben, werden Sie\n' +
                    'wieder von uns nochmals Kontaktiert um die letzten Details abzusprechen.',
                link: null
            },
            {
                expanded: false,
                title: 'Lieferung des Produkts',
                content: 'Sie haben danach selbstverständlich die Möglichkeit sich das Produkt\n' +
                    'selber abzuholen oder es zuschicken zulassen. Die Versandkosten jeden\n' +
                    'Produktes sind kostenlos.',
                link: null
            },
            {
                expanded: false,
                title: 'Unsere Partner',
                content: '(Hier werden alle unsere Partner eingetragen. Autohäuser,\n' +
                    'Immobilienunternehmen, Hotelkette usw.)',
                link: null
            },
            {
                expanded: false,
                title: 'AGB',
                content: 'Hello world',
                link: null
            },
            {
                expanded: false,
                title: 'Datenschutz',
                content: '',
                link: '/privacy'
            },
            {
                expanded: false,
                title: 'Impressum/Kontakt',
                content: '',
                link: '/imprint'
            }
        ];
    }

    ngOnInit() {
    }

    ionViewWillEnter() {
        this.currentUser = this.authService.currentUserValue;
    }

    openMenu() {
        this.menu.enable(true, 'menu');
        this.menu.open('menu');
    }

    expandItem(item): void {
        if (item.link) {
            this.router.navigate([item.link]);
            return;
        }
        if (item.expanded) {
            item.expanded = false;
        } else {
            this.items.map(listItem => {
                if (item === listItem) {
                    listItem.expanded = !listItem.expanded;
                } else {
                    listItem.expanded = false;
                }
                return listItem;
            });
        }
    }

}
