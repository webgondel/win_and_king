import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HlifePageRoutingModule } from './hlife-routing.module';

import { HlifePage } from './hlife.page';
import {ExpandableComponent} from './expandable/expandable.component';
import {MatIconModule} from "@angular/material/icon";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        HlifePageRoutingModule,
        MatIconModule
    ],
  declarations: [HlifePage, ExpandableComponent]
})
export class HlifePageModule {}
