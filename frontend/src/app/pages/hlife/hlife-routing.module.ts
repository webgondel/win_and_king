import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HlifePage } from './hlife.page';

const routes: Routes = [
  {
    path: '',
    component: HlifePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HlifePageRoutingModule {}
