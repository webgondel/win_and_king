import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HlifePage } from './hlife.page';

describe('HlifePage', () => {
  let component: HlifePage;
  let fixture: ComponentFixture<HlifePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HlifePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HlifePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
