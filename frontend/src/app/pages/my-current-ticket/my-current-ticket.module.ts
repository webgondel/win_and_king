import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MyCurrentTicketPageRoutingModule } from './my-current-ticket-routing.module';

import { MyCurrentTicketPage } from './my-current-ticket.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MyCurrentTicketPageRoutingModule
  ],
  declarations: [MyCurrentTicketPage]
})
export class MyCurrentTicketPageModule {}
