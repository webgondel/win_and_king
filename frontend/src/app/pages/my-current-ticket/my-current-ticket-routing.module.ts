import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MyCurrentTicketPage } from './my-current-ticket.page';

const routes: Routes = [
  {
    path: '',
    component: MyCurrentTicketPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MyCurrentTicketPageRoutingModule {}
