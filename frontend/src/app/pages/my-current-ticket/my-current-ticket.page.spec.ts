import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MyCurrentTicketPage } from './my-current-ticket.page';

describe('MyCurrentTicketPage', () => {
  let component: MyCurrentTicketPage;
  let fixture: ComponentFixture<MyCurrentTicketPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyCurrentTicketPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MyCurrentTicketPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
