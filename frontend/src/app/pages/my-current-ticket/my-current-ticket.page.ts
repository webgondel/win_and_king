import {Component, OnInit} from '@angular/core';
import {MenuController} from '@ionic/angular';
import {AuthService} from '../../shared/services/auth/auth.service';
import {HttpService} from '../../shared/services/http/http.service';

@Component({
    selector: 'app-my-current-ticket',
    templateUrl: './my-current-ticket.page.html',
    styleUrls: ['./my-current-ticket.page.scss'],
})
export class MyCurrentTicketPage implements OnInit {

    public currentUser: any;
    public myLose = 0;

    constructor(
        private menu: MenuController,
        public authService: AuthService,
        public httpRequest: HttpService
    ) {
    }

    ngOnInit() {
        this.currentUser = this.authService.currentUserValue;
        this.httpRequest.getMyOwnLose(this.currentUser.id).subscribe((res: any) => {
            this.myLose = res.data;
        });
    }

    openMenu() {
        this.menu.enable(true, 'menu');
        this.menu.open('menu');
    }

}
