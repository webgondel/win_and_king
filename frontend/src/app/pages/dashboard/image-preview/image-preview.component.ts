import {Component, Input, OnInit} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {ModalController} from '@ionic/angular';

@Component({
    selector: 'app-image-preview',
    templateUrl: './image-preview.component.html',
    styleUrls: ['./image-preview.component.scss'],
})
export class ImagePreviewComponent implements OnInit {
    slideOpts = {
        initialSlide: 0,
        speed: 400
    };
    @Input() image: string;
    BASE_URL = environment.BASE_URL;

    constructor(
        public modalCtrl: ModalController
    ) {
    }

    ngOnInit() {
    }

    dismissModal() {
        this.modalCtrl.dismiss({
        });
    }
}
