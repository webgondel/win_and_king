import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';

import {DashboardPageRoutingModule} from './dashboard-routing.module';

import {DashboardPage} from './dashboard.page';
import {NgxPayPalModule} from 'ngx-paypal';
import {ImagePreviewComponent} from './image-preview/image-preview.component';
import { PaymentGatewayComponent } from 'src/app/payment-gateway/payment-gateway.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        DashboardPageRoutingModule,
        NgxPayPalModule
    ],
    declarations: [DashboardPage, ImagePreviewComponent, PaymentGatewayComponent],
    entryComponents: [ImagePreviewComponent]
})
export class DashboardPageModule {
}
