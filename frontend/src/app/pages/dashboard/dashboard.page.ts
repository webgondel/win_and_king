import {Component, OnInit, ViewChild} from '@angular/core';
import {AlertController, IonSlides, LoadingController, MenuController, ModalController} from '@ionic/angular';
import {ActivatedRoute} from '@angular/router';
import {HttpService} from '../../shared/services/http/http.service';
import {environment} from '../../../environments/environment';
import {AuthService} from '../../shared/services/auth/auth.service';
import {ImagePreviewComponent} from './image-preview/image-preview.component';


interface Item {
    id: number;
    name: string;
    total_quantity: number;
    additional_quantity: number;
    type: string;
    isComplete: boolean;
    preview_images: string;
    winner: any;
    user: object;
    createdAt: any;
    updatedAt: any;
}

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.page.html',
    styleUrls: ['./dashboard.page.scss'],
})

export class DashboardPage implements OnInit {

    @ViewChild(IonSlides) menuSlider: IonSlides;

    slideOpts = {
        initialSlide: 0,
        speed: 400
    };

    BASE_URL = environment.BASE_URL;

    step = 0;
    items: any;
    tempItems: any;
    currentItem: Item = null;
    currentPreviewImages: any = [];
    index = 0;
    categories = ['car', 'house', 'travel', 'electronic'];
    currentCategory = this.categories[0];
    favourite = false;
    totalQuantity: any;
    percentage: any;
    checkError: boolean;
    currentUser: any;
    quantity: string;

    constructor(
        private menu: MenuController,
        public alertController: AlertController,
        public httpRequest: HttpService,
        public authService: AuthService,
        public loadingController: LoadingController,
        public modalController: ModalController,
        public route: ActivatedRoute,
    ) {
    }

    ionViewWillEnter() {
        this.currentUser = this.authService.currentUserValue;
        this.httpRequest.getItems().subscribe(res => {
            this.items = res;
            if (this.route.snapshot.params.id) {
                const index = this.items.findIndex(item => item.id === parseInt(this.route.snapshot.params.id, 10));
                this.currentCategory = this.items[index].type;
                this.tempItems = this.items.filter(item => item.type === this.currentCategory);
                this.index = this.tempItems.findIndex(item => item.id === parseInt(this.route.snapshot.params.id, 10));
            } else {
                this.tempItems = this.items.filter(item => item.type === this.currentCategory);
            }
            this.selectCurrentItem();
        }, error => {
        });
    }

    ngOnInit() {
    }

    async selectCurrentItem() {
        this.currentItem = null;
        this.currentPreviewImages = [];
        if (this.tempItems.length > 0) {
            this.currentItem = this.tempItems[this.index];
            this.currentPreviewImages = JSON.parse(this.currentItem.preview_images);
            const loader = await this.loadingController.create();
            loader.present();

            this.httpRequest.getTotalQuantity(this.currentItem.id).subscribe((res: any) => {
                if (res.total === null) {
                    this.totalQuantity = 0;
                } else {
                    this.totalQuantity = res.total + this.currentItem.additional_quantity;
                }
                this.percentage = (this.totalQuantity / this.currentItem.total_quantity * 100 > 100 ?
                    100 : this.totalQuantity / this.currentItem.total_quantity * 100).toFixed(1);

                this.httpRequest.getFavourite({
                    user_id: this.currentUser.id,
                    item_id: this.currentItem.id
                }).subscribe(result => {
                    this.favourite = result !== null;
                    loader.dismiss();
                });

            });
            this.menuSlider.slideTo(0);
        }
    }

    nextItem() {
        if (this.index === this.tempItems.length - 1) {
            return;
        }
        this.index++;
        this.selectCurrentItem();
    }

    prevItem() {
        if (this.index === 0) {
            return;
        }
        this.index--;
        this.selectCurrentItem();
    }

    openMenu() {
        this.menu.enable(true, 'menu');
        this.menu.open('menu');
    }

    async showInfo(item) {
        if (this.currentItem) {
            const alert = await this.alertController.create({
                header: item.name,
                subHeader: 'Total Cost: ' + item.total_quantity + '€',
                message: '<p style="white-space: pre-wrap;">' + item.description + '</p>',
                buttons: ['Close']
            });

            await alert.present();
        }
    }

    setFavourite(item) {
        if (this.currentItem) {
            if (this.favourite) {
                this.httpRequest.removeFavourite({user_id: this.currentUser.id, item_id: item.id}).subscribe(res => {
                    this.favourite = false;
                });
            } else {
                this.httpRequest.setFavourite({user_id: this.currentUser.id, item_id: item.id}).subscribe(res => {
                    this.favourite = true;
                });
            }
        }
    }

    filterItem(i) {
        this.currentCategory = this.categories[i];
        this.tempItems = this.items.filter(item => {
            return item.type === this.currentCategory;
        });
        this.index = 0;
        this.selectCurrentItem();
        this.step = 0;
    }

    prevSlide() {
        this.menuSlider.slidePrev();
    }

    nextSlide() {
        this.menuSlider.slideNext();
    }

    nextStep(n) {
        if (this.currentItem) {
            if (this.currentItem.isComplete) {
                return;
            }
            this.step = n;
        }
    }

    goBack() {
        this.step--;
    }

    checkValue() {
        this.checkError = Number(this.quantity) > (this.currentItem.total_quantity - this.totalQuantity);
    }

    // on payment success -- increase ticket
    handleTickets(item) {
        this.totalQuantity += this.quantity;
        this.percentage = (this.totalQuantity / this.currentItem.total_quantity * 100 > 100 ?
            100 : this.totalQuantity / this.currentItem.total_quantity * 100).toFixed(1);
        this.currentItem = item;
        this.nextStep(1);
    }

    async zoomIn(image) {
        const modal = await this.modalController.create({
            component: ImagePreviewComponent,
            componentProps: {
                image
            }
        });

        return await modal.present();
    }

}
