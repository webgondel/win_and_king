import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MenuController, NavController} from '@ionic/angular';
import {AuthService} from '../../shared/services/auth/auth.service';

@Component({
    selector: 'app-forgot',
    templateUrl: './forgot.page.html',
    styleUrls: ['./forgot.page.scss'],
})
export class ForgotPage implements OnInit {

    isSent = false;
    public forgotForm: FormGroup;
    public error: any = null;

    constructor(
        public formBuilder: FormBuilder,
        private menu: MenuController,
        public navCtrl: NavController,
        public authService: AuthService
    ) {
    }

    ngOnInit() {
        if (this.authService.currentUserValue) {
            this.navCtrl.navigateRoot('dashboard');
        }

        this.forgotForm = this.formBuilder.group({
            email: [null, Validators.compose([
                Validators.required, Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-z]{2,4}$')
            ])]
        });
    }

    openMenu() {
        this.menu.enable(true, 'menu');
        this.menu.open('menu');
    }

    goBack() {
        this.navCtrl.pop();
    }

    resetPassword() {
        this.authService.resetPassword(this.forgotForm.controls.email.value).subscribe(res => {
            this.isSent = true;
            this.error = null;
        }, error => {
            this.error = error.error.message;
        });
    }

    gotoLogin() {
        this.navCtrl.navigateRoot('login');
        this.isSent = false;
    }

}
