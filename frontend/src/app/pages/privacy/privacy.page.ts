import {Component, OnInit} from '@angular/core';
import {MenuController} from '@ionic/angular';
import {AuthService} from '../../shared/services/auth/auth.service';

@Component({
    selector: 'app-privacy',
    templateUrl: './privacy.page.html',
    styleUrls: ['./privacy.page.scss'],
})
export class PrivacyPage implements OnInit {

    currentUser: any;

    constructor(
        private menu: MenuController,
        public authService: AuthService,
    ) {
    }

    ngOnInit() {
    }

    ionViewWillEnter() {
        this.currentUser = this.authService.currentUserValue;
    }

    openMenu() {
        this.menu.enable(true, 'menu');
        this.menu.open('menu');
    }

}
