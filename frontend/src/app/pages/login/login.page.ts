import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {MenuController, NavController} from '@ionic/angular';

import {AuthService} from '../../shared/services/auth/auth.service';
import {first} from 'rxjs/operators';
import {environment} from '../../../environments/environment';

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

    public loginForm: FormGroup;
    public error: any = null;

    constructor(
        public formBuilder: FormBuilder,
        public router: Router,
        private menu: MenuController,
        public navCtrl: NavController,
        public authService: AuthService
    ) {
    }

    ngOnInit() {
        if (this.authService.currentUserValue) {
            this.navCtrl.navigateRoot('dashboard');
        }

        this.loginForm = this.formBuilder.group({
            email: [null, Validators.compose([
                Validators.required,
                Validators.pattern('^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$')
            ])],
            password: [null, Validators.compose([
                Validators.required
            ])]
        });
    }

    openMenu() {
        this.menu.enable(true, 'menu');
        this.menu.open('menu');
    }

    login() {
        if (!this.loginForm.valid) {
            return;
        }

        this.authService.login(this.f.email.value, this.f.password.value).pipe(first()).subscribe(
            res => {
                this.f.password.setValue(null);
                if (res.isAdmin === true) {
                    this.router.navigate(['/admin/create_item']);
                } else {
                    this.router.navigate(['/dashboard']);
                }
            },
            error => {
                console.log(error);
                this.error = error.error.message;
            }
        );
    }

    get f() {
        return this.loginForm.controls;
    }

    goBack() {
        this.navCtrl.pop();
    }
}
