import {Component, OnInit} from '@angular/core';
import {MenuController} from '@ionic/angular';
import {AuthService} from '../../shared/services/auth/auth.service';

@Component({
    selector: 'app-imprint',
    templateUrl: './imprint.page.html',
    styleUrls: ['./imprint.page.scss'],
})
export class ImprintPage implements OnInit {

    currentUser: any;

    constructor(
        private menu: MenuController,
        public authService: AuthService,
    ) {
    }

    ngOnInit() {
    }

    ionViewWillEnter() {
        this.currentUser = this.authService.currentUserValue;
    }

    openMenu() {
        this.menu.enable(true, 'menu');
        this.menu.open('menu');
    }

}
