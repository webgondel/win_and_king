import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MyBidsPage } from './my-bids.page';

describe('MyBidsPage', () => {
  let component: MyBidsPage;
  let fixture: ComponentFixture<MyBidsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyBidsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MyBidsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
