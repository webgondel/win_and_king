import {Component, OnInit} from '@angular/core';
import {MenuController} from '@ionic/angular';
import {AuthService} from '../../shared/services/auth/auth.service';
import {HttpService} from '../../shared/services/http/http.service';
import {environment} from '../../../environments/environment';

@Component({
    selector: 'app-my-bids',
    templateUrl: './my-bids.page.html',
    styleUrls: ['./my-bids.page.scss'],
})
export class MyBidsPage implements OnInit {

    public currentUser: any;
    public itemLists: any;

    public BASE_URL = environment.BASE_URL;

    constructor(
        private menu: MenuController,
        public authService: AuthService,
        public httpRequest: HttpService
    ) {
    }

    ionViewWillEnter() {
        this.currentUser = this.authService.currentUserValue;
        this.httpRequest.getAllMyBids(this.currentUser.id).subscribe(res => {
            this.itemLists = res;
        });
    }

    ngOnInit() {
    }

    openMenu() {
        this.menu.enable(true, 'menu');
        this.menu.open('menu');
    }

    getImageFileName(item) {
        return JSON.parse(item.preview_images)[0];
    }

    formatDate(d) {
        const date = new Date(d);
        let dd = date.getDate().toString();
        let mm = (date.getMonth() + 1).toString();
        const yyyy = date.getFullYear();
        if (date.getDate() < 10) {
            dd = '0' + dd;
        }
        if (date.getMonth() < 9) {
            mm = '0' + mm;
        }
        return d = dd + '.' + mm + '.' + yyyy;
    }
}
