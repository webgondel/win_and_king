import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../shared/services/auth/auth.service';
import {MenuController} from '@ionic/angular';
import {PayPal, PayPalConfiguration, PayPalPayment} from '@ionic-native/paypal/ngx';
import {HttpService} from '../../shared/services/http/http.service';

@Component({
    selector: 'app-buying-lose',
    templateUrl: './buying-lose.page.html',
    styleUrls: ['./buying-lose.page.scss'],
})
export class BuyingLosePage implements OnInit {
    public currentUser: any;
    public step = 0;
    public quantity: any;

    constructor(
        public authService: AuthService,
        private menu: MenuController,
        public payPal: PayPal,
        public httpRequest: HttpService,
    ) {
    }

    ngOnInit() {
        this.currentUser = this.authService.currentUserValue;
    }

    openMenu() {
        this.menu.enable(true, 'menu');
        this.menu.open('menu');
    }

    nextStep(value) {
        this.step = value;
    }

    goBack() {
        this.step--;
    }

    onlinePayByPaypal() {
        this.payPal.init({
            PayPalEnvironmentProduction: '',
            PayPalEnvironmentSandbox: 'AUMlvDHJ7S3b7zJZZIqoz8rRtUNdJOiGOnAYsdgqw3zCvj4aJlfJ18cBAO3XYaidJpi2WXYcxTIQCgML'
        }).then(() => {
            this.payPal.prepareToRender('PayPalEnvironmentSandbox', new PayPalConfiguration({})).then(() => {
                const payment = new PayPalPayment(this.quantity, 'EUR', 'Description', 'sale');
                this.payPal.renderSinglePaymentUI(payment).then((res) => {
                    this.httpRequest.onlineBuyLoseByPaypal({
                        user_id: this.currentUser.id,
                        quantity: this.quantity
                    }).subscribe((response: any) => {
                        this.nextStep(0);
                        this.quantity = null;
                    });
                }, (err) => {
                    console.log(err);
                });
            }, (err) => {
                console.log(err);
            });
        }, (err) => {
            console.log(err);
        });
    }

}
