import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BuyingLosePage } from './buying-lose.page';

describe('BuyingLosePage', () => {
  let component: BuyingLosePage;
  let fixture: ComponentFixture<BuyingLosePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuyingLosePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BuyingLosePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
