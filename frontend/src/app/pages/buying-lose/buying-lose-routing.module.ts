import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BuyingLosePage } from './buying-lose.page';

const routes: Routes = [
  {
    path: '',
    component: BuyingLosePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BuyingLosePageRoutingModule {}
