import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BuyingLosePageRoutingModule } from './buying-lose-routing.module';

import { BuyingLosePage } from './buying-lose.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BuyingLosePageRoutingModule
  ],
  declarations: [BuyingLosePage]
})
export class BuyingLosePageModule {}
