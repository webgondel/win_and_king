import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MenuController, NavController, ToastController} from '@ionic/angular';
import {AuthService} from '../../shared/services/auth/auth.service';
import {first} from 'rxjs/operators';
import {MustMatch} from '../../shared/helper/mustmatch';

@Component({
    selector: 'app-register',
    templateUrl: './register.page.html',
    styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

    public registerForm: FormGroup;
    public error: any = null;

    constructor(
        public formBuilder: FormBuilder,
        private menu: MenuController,
        public navCtrl: NavController,
        public authService: AuthService,
        public toastController: ToastController
    ) {
    }

    ngOnInit() {

        if (this.authService.currentUserValue) {
            this.navCtrl.navigateRoot('dashboard');
        }

        this.registerForm = this.formBuilder.group({
            email: [null, Validators.compose([
                Validators.required, Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-z]{2,4}$')
            ])],
            password: [null, Validators.compose([
                Validators.required
            ])],
            confirmPassword: [null, Validators.compose([
                Validators.required
            ])],
            username: [null, Validators.compose([
                Validators.required
            ])],
        }, {
            validator: MustMatch('password', 'confirmPassword')
        });
    }

    openMenu() {
        this.menu.enable(true, 'menu');
        this.menu.open('menu');
    }

    goBack() {
        this.navCtrl.pop();
    }

    get f() {
        return this.registerForm.controls;
    }

    register() {
        if (!this.registerForm.valid) {
            return;
        }

        const user = {
            email: this.f.email.value.toLowerCase(),
            password: this.f.password.value,
            username: this.f.username.value
        };

        this.authService.register(user).pipe(first()).subscribe(res => {
            this.navCtrl.navigateRoot('/login');
            this.presentToast();
        }, error => {
            console.log(error);
            this.error = error.error.message;
        });
    }

    async presentToast() {
        const toast = await this.toastController.create({
            message: 'User was registered successfully.',
            duration: 2000
        });
        toast.present();
    }
}
