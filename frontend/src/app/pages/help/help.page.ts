import {Component, OnInit} from '@angular/core';
import {MenuController} from "@ionic/angular";
import {AuthService} from "../../shared/services/auth/auth.service";

@Component({
    selector: 'app-help',
    templateUrl: './help.page.html',
    styleUrls: ['./help.page.scss'],
})
export class HelpPage implements OnInit {


    constructor(
        private menu: MenuController,
    ) {
    }

    ngOnInit() {
    }

    ionViewWillEnter() {
    }

    openMenu() {
        this.menu.enable(true, 'menu');
        this.menu.open('menu');
    }

}
