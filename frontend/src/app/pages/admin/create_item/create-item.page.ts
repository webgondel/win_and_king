import {Component, OnInit} from '@angular/core';
import {LoadingController, MenuController} from '@ionic/angular';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import imageCompression from 'browser-image-compression';

import {HttpService} from '../../../shared/services/http/http.service';
import {LoadingService} from '../../../shared/services/loading/loading.service';
import {AuthService} from '../../../shared/services/auth/auth.service';

@Component({
    selector: 'app-create-item',
    templateUrl: './create-item.page.html',
    styleUrls: ['./create-item.page.scss'],
})
export class CreateItemPage implements OnInit {

    public types = [
        {
            value: 'car',
            viewValue: 'Car'
        },
        {
            value: 'house',
            viewValue: 'House'
        },
        {
            value: 'travel',
            viewValue: 'Travel'
        },
        {
            value: 'electronic',
            viewValue: 'Electronic'
        }
    ];
    public createItemForm: FormGroup;
    public fileLists: any[] = [];
    public currentUser: any;

    constructor(
        public formBuilder: FormBuilder,
        private menu: MenuController,
        public httpRequest: HttpService,
        public loadingController: LoadingController,
        public authService: AuthService
    ) {
    }

    async ngOnInit() {
        this.currentUser = this.authService.currentUserValue;
        this.createItemForm = this.formBuilder.group({
            type: [null, Validators.compose([
                Validators.required
            ])],
            total_quantity: [null, Validators.compose([
                Validators.required
            ])],
            name: [null, Validators.compose([
                Validators.required
            ])],
            description: [null],
            preview_images: [null, Validators.compose([
                Validators.required
            ])]
        });
    }

    get f() {
        return this.createItemForm.controls;
    }

    openMenu() {
        this.menu.enable(true, 'menu');
        this.menu.open('menu');
    }

    async saveItem() {
        const fd: FormData = new FormData();
        for (const item of this.fileLists) {
            const options = {
                maxSizeMB: 0.5,
                maxWidthOrHeight: 1920,
                useWebWorker: true
            };
            const compressedFile = await imageCompression(item, options);
            fd.append('preview_images', compressedFile);
        }
        fd.append('type', this.f.type.value);
        fd.append('total_quantity', this.f.total_quantity.value);
        fd.append('name', this.f.name.value);
        fd.append('isComplete', 'false');
        fd.append('description', this.f.description.value);

        const loader = await this.loadingController.create();
        loader.present();

        this.httpRequest.createItem(fd).subscribe(res => {
            this.createItemForm.reset();
            loader.dismiss();
            this.fileLists = [];
        }, error => {
            loader.dismiss();
            console.log(error);
        });
    }

    onChange(event) {
        for (const item of event) {
            this.fileLists.push(item);
        }
    }

}
