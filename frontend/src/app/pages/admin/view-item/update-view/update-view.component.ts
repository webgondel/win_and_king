import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LoadingController, ModalController} from '@ionic/angular';
import {HttpService} from '../../../../shared/services/http/http.service';
import {environment} from '../../../../../environments/environment';

@Component({
    selector: 'app-update-view',
    templateUrl: './update-view.component.html',
    styleUrls: ['./update-view.component.scss'],
})
export class UpdateViewComponent implements OnInit {

    @Input() product: any;
    public updateItemForm: FormGroup;
    public fileLists: any[] = [];
    public BASE_URL = environment.BASE_URL;
    public removedImages = [];
    public types = [
        {
            value: 'car',
            viewValue: 'Car'
        },
        {
            value: 'house',
            viewValue: 'House'
        },
        {
            value: 'travel',
            viewValue: 'Travel'
        },
        {
            value: 'electronic',
            viewValue: 'Electronic'
        }
    ];
    public totalQuantity;
    public percentage;

    constructor(
        public formBuilder: FormBuilder,
        public modalCtrl: ModalController,
        public httpService: HttpService,
        public loadingController: LoadingController
    ) {
    }

    ngOnInit() {
        console.log(this.product);
        this.httpService.getTotalQuantity(this.product.id).subscribe((res: any) => {
            console.log(res);
            this.totalQuantity = res.total + this.product.additional_quantity;
            this.percentage = (this.totalQuantity / this.product.total_quantity * 100 > 100 ?
                100 : this.totalQuantity / this.product.total_quantity * 100).toFixed(1);
            const minPercent = (res.total / this.product.total_quantity * 100 > 100 ?
                100 : this.totalQuantity / this.product.total_quantity * 100).toFixed(1);
            this.f.percentage.setValidators(Validators.compose([
                Validators.min(parseFloat(minPercent)),
                Validators.max(100)
            ]));
            this.f.percentage.setValue(this.percentage);
            console.log('percentage >>>>>>>> ', this.percentage);
        });
        this.updateItemForm = this.formBuilder.group({
            type: [this.product.type, Validators.compose([
                Validators.required
            ])],
            total_quantity: [this.product.total_quantity, Validators.compose([
                Validators.required
            ])],
            percentage: [null],
            name: [this.product.name, Validators.compose([
                Validators.required
            ])],
            description: [this.product.description],
            preview_images: [null, Validators.compose([])]
        });
    }

    get f(): any {
        return this.updateItemForm.controls;
    }

    dismissModal() {
        this.modalCtrl.dismiss();
    }

    onChange(event) {
        for (const item of event) {
            this.fileLists.push(item);
        }
    }

    async updateItem() {
        if (this.updateItemForm.invalid) {
            return;
        }
        const fd: FormData = new FormData();
        for (const item of this.fileLists) {
            fd.append('preview_images', item);
        }
        const auctionTotalPercentage = (this.totalQuantity - this.product.additional_quantity) / this.product.total_quantity * 100;
        const percentage = Math.round((this.f.percentage.value - auctionTotalPercentage) * this.product.total_quantity / 100);
        fd.append('type', this.f.type.value);
        fd.append('total_quantity', this.f.total_quantity.value);
        fd.append('percentage', percentage.toString());
        fd.append('name', this.f.name.value);
        fd.append('description', this.f.description.value);
        fd.append('old_images', JSON.stringify(this.product.preview_images));

        const loader = await this.loadingController.create();
        loader.present();

        this.httpService.updateItem(fd, this.product.id).subscribe(res => {
            loader.dismiss();
            this.modalCtrl.dismiss(res);
        });

        // this.httpService.createItem(fd).subscribe(res => {
        //   loader.dismiss();
        //   this.fileLists = [];
        // });
    }

    deleteImage(fileName) {
        console.log(fileName);
        console.log(this.product);
        const index = this.product.preview_images.findIndex(item => item === fileName);
        console.log(index);
        console.log(this.product.preview_images.splice(index, 1));

    }
}
