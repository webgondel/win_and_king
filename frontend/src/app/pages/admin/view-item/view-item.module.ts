import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';

import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatIconModule} from '@angular/material/icon';
import {MatDialogModule} from '@angular/material/dialog';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';

import {ViewItemPageRoutingModule} from './view-item-routing.module';
import {ViewItemPage} from './view-item.page';
import {UpdateViewComponent} from './update-view/update-view.component';
import {PurchaseHistoryComponent} from './purchase-history/purchase-history.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ViewItemPageRoutingModule,
        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        MatIconModule,
        MatDialogModule,
        MatCardModule,
        MatFormFieldModule,
        MatSelectModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatInputModule
    ],
    declarations: [ViewItemPage, UpdateViewComponent, PurchaseHistoryComponent],
    entryComponents: [UpdateViewComponent, PurchaseHistoryComponent]
})
export class ViewItemPageModule {
}
