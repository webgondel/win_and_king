import {Component, OnInit, ViewChild} from '@angular/core';
import {AlertController, MenuController, ModalController} from '@ionic/angular';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';

import {AuthService} from '../../../shared/services/auth/auth.service';
import {HttpService} from '../../../shared/services/http/http.service';
import {UpdateViewComponent} from './update-view/update-view.component';
import {PurchaseHistoryComponent} from './purchase-history/purchase-history.component';

@Component({
    selector: 'app-view-item',
    templateUrl: './view-item.page.html',
    styleUrls: ['./view-item.page.scss'],
})
export class ViewItemPage implements OnInit {

    public currentUser;
    public allProducts = [];
    displayedColumns = ['type', 'name', 'total_quantity', 'description', 'action'];
    dataSource: any = new MatTableDataSource<any>([]);
    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
    @ViewChild(MatSort, {static: true}) sort: MatSort;

    constructor(
        public authService: AuthService,
        public httpService: HttpService,
        public menu: MenuController,
        public modalController: ModalController,
        public alertController: AlertController
    ) {
    }

    ngOnInit() {
        this.currentUser = this.authService.currentUserValue;
        this.httpService.getItems().subscribe((res: any) => {
            this.allProducts = res;
            this.dataSource.data = res;
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
        });
    }

    openMenu() {
        this.menu.enable(true, 'menu');
        this.menu.open('menu');
    }

    async editProduct(product) {
        const previewImages = JSON.parse(product.preview_images);
        const modal = await this.modalController.create({
            component: UpdateViewComponent,
            componentProps: {
                product: {...product, preview_images: previewImages}
            }
        });

        modal.onDidDismiss().then((res: any) => {
            if (res.data) {
                const index = this.allProducts.findIndex(item => item.id === res.data.id);
                this.allProducts[index] = res.data;
                this.dataSource.data = this.allProducts;
            }
        });

        return await modal.present();
    }

    async purchaseHistory(product) {
        const modal = await this.modalController.create({
            component: PurchaseHistoryComponent,
            componentProps: {
                product
            }
        });

        modal.onDidDismiss().then((res: any) => {
        });

        return await modal.present();
    }

    async deleteProduct(product) {
        const alert = await this.alertController.create({
            header: 'Confirm',
            message: 'Are you sure to delete this product?',
            buttons: [
                {
                    text: 'OK',
                    handler: () => {
                        this.httpService.deleteItem(product.id).subscribe((res: any) => {
                            const index = this.allProducts.findIndex(item => item.id === product.id);
                            this.allProducts.splice(index, 1);
                            this.dataSource.data = this.allProducts;
                        });
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary'
                }
            ]
        });
        await alert.present();
    }
}
