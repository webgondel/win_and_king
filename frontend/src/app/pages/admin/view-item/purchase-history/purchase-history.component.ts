import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {ModalController} from '@ionic/angular';

import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import * as moment from 'moment';

import {HttpService} from '../../../../shared/services/http/http.service';


@Component({
  selector: 'app-purchase-history',
  templateUrl: './purchase-history.component.html',
  styleUrls: ['./purchase-history.component.scss'],
})
export class PurchaseHistoryComponent implements OnInit {

  @Input() product: any;
  displayedColumns = ['no', 'username', 'email', 'timestamp', 'total'];
  dataSource: any = new MatTableDataSource<any>([]);
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  allHistory = [];
  currentPage = 0;
  pageSize = 5;
  constructor(
      public modalCtrl: ModalController,
      public httpService: HttpService,
  ) { }

  ngOnInit() {
    this.httpService.getPurchaseHistory(this.product).subscribe((res: any) => {
      this.allHistory = res;
      for (const history of this.allHistory) {

      }
      this.dataSource.data = res;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  dismissModal() {
    this.modalCtrl.dismiss();
  }

  getTimeFormat(time) {
    moment.locale('de');
    return moment(time).format('DD.MM.YYYY HH:mm');
  }

  onPaginateChange = ($event: PageEvent) => {
    this.currentPage = $event.pageIndex;
    this.pageSize = $event.pageSize;
  }

}
