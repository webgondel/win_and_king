import {Component, OnInit} from '@angular/core';
import {MenuController, NavController} from '@ionic/angular';
import {AuthService} from '../../shared/services/auth/auth.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.page.html',
    styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

    constructor(
        private menu: MenuController,
        public authService: AuthService,
        public navCtrl: NavController
    ) {
    }

    ngOnInit() {
        if (this.authService.currentUserValue) {
            this.navCtrl.navigateRoot('dashboard');
        }
    }

    openMenu() {
        this.menu.enable(true, 'menu');
        this.menu.open('menu');
    }

    openSocialLink(type) {
        if (type === 'facebook') {
            window.open('https://www.facebook.com/Win-And-King-107576854392562');
        } else if (type === 'youtube') {
            window.open('https://www.youtube.com/channel/UCMTr1Nw8E7Slcle8M1A6mfA');
        } else if (type === 'instagram') {
            window.open('https://www.instagram.com/winandking/');
        }
    }
    // https://www.instagram.com/winandking/
    //     https://www.facebook.com/Win-And-King-107576854392562
    // https://www.youtube.com/channel/UCMTr1Nw8E7Slcle8M1A6mfA

}
