import {Injectable} from '@angular/core';
import {LoadingController} from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class LoadingService {

    constructor(
        public loadingController: LoadingController
    ) {
    }

    async showLoader() {
        return await this.loadingController.create({
            message: 'Please wait...'
        });
    }

    async hideLoader(loader) {
        await loader.dismiss().then(() => {
        }).catch(error => {
            console.log(error);
        });
    }
}
