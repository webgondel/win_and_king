import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {URL_JSON} from '../../config/url_json';

@Injectable({
    providedIn: 'root'
})
export class HttpService {

    constructor(
        public http: HttpClient
    ) {
    }

    payTicket(fd) {
        return this.http.post<any>(`${URL_JSON.PAYMENT}/pay-ticket`, fd).toPromise();
    }

    createItem(fd) {
        return this.http.post<any>(`${URL_JSON.ITEM}/create_item`, fd);
    }

    updateItem(data, id) {
        return this.http.put<any>(`${URL_JSON.ITEM}/update_item/${id}`, data);
    }

    getItems() {
        return this.http.get<any>(`${URL_JSON.ITEM}/get_items`);
    }

    deleteItem(id) {
        return this.http.delete<any>(`${URL_JSON.ITEM}/delete_item/${id}`);
    }

    getFavourite(data) {
        return this.http.get<any>(`${URL_JSON.FAVOURITE}/get_favourite/${data.user_id}/${data.item_id}`);
    }

    getAllFavourite(userId) {
        return this.http.get<any>(`${URL_JSON.FAVOURITE}/get_all_favourites/${userId}`);
    }

    removeFavourite(data) {
        return this.http.delete(`${URL_JSON.FAVOURITE}/remove_favourite/${data.user_id}/${data.item_id}`);
    }

    setFavourite(data) {
        return this.http.post(`${URL_JSON.FAVOURITE}/set_favourite`, data);
    }

    onlineBuyLoseByPaypal(data) {
        return this.http.post(`${URL_JSON.AUCTION}/buy_lose`, data);
    }

    getTotalQuantity(id) {
        return this.http.get(`${URL_JSON.AUCTION}/get_total_quantity/${id}`);
    }

    getAllMyBids(id) {
        return this.http.get(`${URL_JSON.AUCTION}/get_all_bids/${id}`);
    }

    getMyOwnLose(id) {
        return this.http.get(`${URL_JSON.AUCTION}/getmyownlose/${id}`);
    }

    getPurchaseHistory(data) {
        return this.http.get(`${URL_JSON.AUCTION}/getPurchaseHistory/${data.id}`);
    }
}
