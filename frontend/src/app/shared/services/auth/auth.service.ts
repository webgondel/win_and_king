import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {NavController} from '@ionic/angular';

import {BehaviorSubject, Observable} from 'rxjs';
import * as jwt_decode from 'jwt-decode';
import {map} from 'rxjs/operators';

import {URL_JSON} from '../../config/url_json';

@Injectable({
    providedIn: 'root'
})

export class AuthService {

    public currentUserSubject: BehaviorSubject<any>;
    public currentUser: Observable<any>;

    constructor(
        public http: HttpClient,
        public navCtrl: NavController
    ) {
        if (localStorage.getItem('currentUser') !== null) {
            this.currentUserSubject = new BehaviorSubject<any>(jwt_decode(localStorage.getItem('currentUser')));
            this.currentUser = this.currentUserSubject.asObservable();
        } else {
            this.currentUserSubject = new BehaviorSubject<any>(null);
            this.currentUser = this.currentUserSubject.asObservable();
        }
    }

    public get currentUserValue(): any {
        return this.currentUserSubject.value;
    }

    public setCurrentUser(currentUser) {
        this.currentUserSubject.next(currentUser);
    }

    login(email: string, password: string) {
        return this.http.post(`${URL_JSON.AUTH}/login`, {email, password}).pipe(
            map((res: any) => {
                if (res) {
                    localStorage.setItem('currentUser', res.token);
                    this.currentUserSubject.next(jwt_decode(res.token));
                }
                return jwt_decode(res.token);
            })
        );
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
        this.navCtrl.navigateRoot('home');
    }

    register(user) {
        return this.http.post<any>(`${URL_JSON.AUTH}/register`, user)
            .pipe(map(response => {
                return response;
            }));
    }

    resetPassword(email) {
        return this.http.put<any>(`${URL_JSON.AUTH}/resetPassword`, {email});
    }

    updateUserInfo(info) {
        return this.http.put(`${URL_JSON.AUTH}/updateUserInfo`, info).pipe(
            map((res: any) => {
                if (res) {
                    localStorage.setItem('currentUser', res.token);
                    this.currentUserSubject.next(jwt_decode(res.token));
                }
                return jwt_decode(res.token);
            })
        );
    }

    updatePassword(password) {
        return this.http.put(`${URL_JSON.AUTH}/updatePassword`, password).pipe(
            map((res: any) => {
                return res;
            })
        );
    }
}

