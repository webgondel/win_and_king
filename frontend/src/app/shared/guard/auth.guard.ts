import {Injectable} from '@angular/core';
import {Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';

import {AuthService} from '../services/auth/auth.service';

@Injectable({providedIn: 'root'})

export class AuthGuard implements CanActivate {

    constructor(
        private router: Router,
        public authService: AuthService
    ) {
    }

    async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const currentUser = this.authService.currentUserValue;
        if (currentUser) {
            if (route.routeConfig.path === 'admin') {
                if (currentUser.isAdmin !== true) {
                    await this.router.navigate(['/home']);
                    return false;
                }
            }
            // await this.router.navigate(['/dashboard']);
            return true;
        }
        // not logged in so redirect to login page with the return url
        await this.router.navigate(['/home']);
        return false;
    }
}
