import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {AuthGuard} from './shared/guard/auth.guard';

const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: '',
                redirectTo: 'home',
                pathMatch: 'full'
            },
            {
                path: 'home',
                loadChildren: () => import('./pages/home/home.module').then(m => m.HomePageModule)
            },
            {
                path: 'login',
                loadChildren: () => import('./pages/login/login.module').then(m => m.LoginPageModule)
            },
            {
                path: 'register',
                loadChildren: () => import('./pages/register/register.module').then(m => m.RegisterPageModule)
            },
            {
                path: 'forgot',
                loadChildren: () => import('./pages/forgot/forgot.module').then(m => m.ForgotPageModule)
            },
            {
                path: 'imprint',
                loadChildren: () => import('./pages/imprint/imprint.module').then(m => m.ImprintPageModule)
            },
            {
                path: 'privacy',
                loadChildren: () => import('./pages/privacy/privacy.module').then(m => m.PrivacyPageModule)
            },
        ],
        // canActivate: [!AuthGuard]
    },
    {
        path: '',
        children: [
            {
                path: 'dashboard',
                loadChildren: () => import('./pages/dashboard/dashboard.module').then(m => m.DashboardPageModule)
            },
            {
                path: 'current-ticket',
                loadChildren: () => import('./pages/my-current-ticket/my-current-ticket.module').then(m => m.MyCurrentTicketPageModule)
            },
            {
                path: 'bids',
                loadChildren: () => import('./pages/my-bids/my-bids.module').then(m => m.MyBidsPageModule)
            },
            {
                path: 'favourite',
                loadChildren: () => import('./pages/favourite/favourite.module').then(m => m.FavouritePageModule)
            },
            {
                path: 'profile',
                loadChildren: () => import('./pages/profile/profile.module').then(m => m.ProfilePageModule)
            },
            {
                path: 'buying-lose',
                // loadChildren: () => import('./pages/buying-lose/buying-lose.module').then(m => m.BuyingLosePageModule)
                loadChildren: () => import('./pages/dashboard/dashboard.module').then(m => m.DashboardPageModule)
            }
        ],
        canActivate: [AuthGuard],
        data: {isAdmin: false}
    },
    {
        path: 'admin',
        children: [
            {
                path: 'create_item',
                loadChildren: () => import('./pages/admin/create_item/create-item.module').then(m => m.CreateItemPageModule),
            },
            {
                path: 'view_item',
                loadChildren: () => import('./pages/admin/view-item/view-item.module').then(m => m.ViewItemPageModule)
            }
        ],
        canActivate: [AuthGuard],
        data: {isAdmin: true}
    },
    {
        path: 'help',
        loadChildren: () => import('./pages/help/help.module').then(m => m.HelpPageModule)
    },
    {
        path: 'hlife',
        loadChildren: () => import('./pages/hlife/hlife.module').then(m => m.HlifePageModule)
    },
    {
        path: 'view-item',
        loadChildren: () => import('./pages/admin/view-item/view-item.module').then(m => m.ViewItemPageModule)
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})
    ],
    exports: [RouterModule]
})

export class AppRoutingModule {
}
