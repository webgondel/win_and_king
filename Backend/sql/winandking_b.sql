-- MySQL dump 10.13  Distrib 5.7.30, for Linux (x86_64)
--
-- Host: localhost    Database: winandking
-- ------------------------------------------------------
-- Server version	5.7.30-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auctions`
--

DROP TABLE IF EXISTS `auctions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auctions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auctions`
--

LOCK TABLES `auctions` WRITE;
/*!40000 ALTER TABLE `auctions` DISABLE KEYS */;
INSERT INTO `auctions` VALUES (1,1,1,500,'2020-10-06 15:49:21','2020-10-06 15:49:21'),(2,4,1,500,'2020-10-06 15:49:39','2020-10-06 15:49:39'),(3,3,2,2,'2020-10-07 07:06:12','2020-10-07 07:06:12'),(4,3,2,2,'2020-10-07 07:06:25','2020-10-07 07:06:25'),(5,3,2,6,'2020-10-07 09:19:51','2020-10-07 09:19:51'),(6,3,2,2,'2020-10-12 07:22:25','2020-10-12 07:22:25'),(7,3,4,2000,'2020-10-13 06:37:00','2020-10-13 06:37:00'),(8,3,4,220,'2020-10-13 15:43:49','2020-10-13 15:43:49'),(9,3,3,2222,'2020-10-13 15:46:38','2020-10-13 15:46:38'),(10,3,4,330,'2020-10-14 10:10:18','2020-10-14 10:10:18'),(11,3,3,956,'2020-10-14 10:13:32','2020-10-14 10:13:32'),(12,3,3,222,'2020-10-15 08:50:47','2020-10-15 08:50:47'),(13,1,9,1,'2020-10-19 14:42:29','2020-10-19 14:42:29'),(14,1,9,1,'2020-10-19 14:42:35','2020-10-19 14:42:35'),(15,3,9,20,'2020-10-21 12:16:06','2020-10-21 12:16:06'),(16,6,12,1,'2020-10-22 21:03:31','2020-10-22 21:03:31'),(17,6,9,1,'2020-12-03 16:50:44','2020-12-03 16:50:44'),(18,6,9,1,'2020-12-03 16:50:47','2020-12-03 16:50:47'),(19,6,11,1,'2020-12-08 19:22:44','2020-12-08 19:22:44'),(20,18,7,1,'2021-01-22 18:47:08','2021-01-22 18:47:08'),(21,18,7,1,'2021-01-22 18:48:19','2021-01-22 18:48:19'),(22,18,15,1,'2021-01-22 19:13:36','2021-01-22 19:13:36'),(23,20,7,1,'2021-01-28 15:17:17','2021-01-28 15:17:17'),(24,1,9,1,'2021-01-28 15:51:47','2021-01-28 15:51:47'),(25,3,9,2,'2021-03-04 16:24:48','2021-03-04 16:24:48'),(26,3,9,10,'2021-03-04 16:26:50','2021-03-04 16:26:50'),(27,3,10,20,'2021-03-04 16:30:04','2021-03-04 16:30:04'),(28,3,9,33,'2021-03-04 16:33:17','2021-03-04 16:33:17'),(29,3,9,2,'2021-03-11 15:06:17','2021-03-11 15:06:17'),(30,3,9,2,'2021-03-11 15:06:27','2021-03-11 15:06:27'),(31,1,9,121,'2021-03-11 15:24:36','2021-03-11 15:24:36');
/*!40000 ALTER TABLE `auctions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `favourites`
--

DROP TABLE IF EXISTS `favourites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `favourites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `favourites`
--

LOCK TABLES `favourites` WRITE;
/*!40000 ALTER TABLE `favourites` DISABLE KEYS */;
INSERT INTO `favourites` VALUES (8,3,3,'2020-10-09 09:58:24','2020-10-09 09:58:24'),(9,3,1,'2020-10-09 09:59:07','2020-10-09 09:59:07'),(11,6,1,'2020-10-12 21:29:28','2020-10-12 21:29:28'),(12,3,4,'2020-10-14 10:10:27','2020-10-14 10:10:27'),(13,1,12,'2020-10-19 14:41:49','2020-10-19 14:41:49'),(18,20,18,'2021-01-28 15:23:25','2021-01-28 15:23:25'),(20,3,13,'2021-03-04 16:31:29','2021-03-04 16:31:29'),(22,3,15,'2021-03-04 16:35:46','2021-03-04 16:35:46'),(23,3,9,'2021-03-11 14:49:55','2021-03-11 14:49:55'),(25,21,7,'2021-04-06 19:28:10','2021-04-06 19:28:10'),(27,21,9,'2021-04-21 09:48:23','2021-04-21 09:48:23');
/*!40000 ALTER TABLE `favourites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('car','house','travel','electronic') DEFAULT NULL,
  `total_quantity` int(11) DEFAULT NULL,
  `isComplete` tinyint(1) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `preview_images` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `winner` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `additional_quantity` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` VALUES (1,'car',1000,1,'123','222','[\"1601999348576.jpg\"]',0,4,'2020-10-06 15:49:08','2020-10-19 14:10:12',0),(2,'house',12,0,'Tolles Haus','Beschreibung','[\"1602054311252.png\"]',0,NULL,'2020-10-07 07:05:11','2020-10-14 08:13:05',0),(3,'house',9000,0,'Luxus Villa Verona','Ein wunderschönes Haus','[\"1602062941047.png\"]',0,NULL,'2020-10-07 09:29:01','2020-10-19 14:10:15',0),(4,'car',4000,0,'Bentley Bentayga','test123','[]',0,NULL,'2020-10-13 06:36:18','2020-10-19 14:10:07',0),(5,'electronic',10000,0,'PlayStation 5 Limited Edition Gol','Play Station 5 Gold Design zwei mal \r\nSp','[\"1603109723211.jpeg\"]',1,NULL,'2020-10-19 12:15:23','2020-12-03 17:13:46',3000),(6,'car',200000,0,'LAMBORGHINI AVENTADOR S','1234567891011213141516171819202122232425','[\"1603116555271.jpeg\"]',0,NULL,'2020-10-19 14:09:15','2020-10-19 14:16:14',0),(7,'house',10000000,0,'Traumhaus 1','null','[\"1603116796164.jpeg\"]',1,NULL,'2020-10-19 14:13:16','2020-11-03 16:10:36',1550000),(8,'house',2000000,0,'Traumhaus2','null','[\"1603116832724.jpeg\"]',1,NULL,'2020-10-19 14:13:52','2020-11-03 09:57:00',1000000),(9,'car',200000,0,'LAMBORGHINI AVENTADOR S','Qwiekekdjddjejejdjdndjdddjdndndjdjdjdndj','[\"1603117043538.jpeg\",\"1603117043545.jpeg\",\"1603117043552.jpeg\"]',1,NULL,'2020-10-19 14:17:23','2020-11-02 09:47:26',40000),(10,'car',300000,0,'LAMBORGHINI HURACÁN EVO','null','[\"1603117293340.jpeg\",\"1603117293349.jpeg\",\"1603117293355.jpeg\"]',1,NULL,'2020-10-19 14:21:33','2020-10-19 14:21:33',0),(11,'car',400000,0,'LAMBORGHINI URUS','null','[\"1603117539085.jpeg\",\"1603117539096.jpeg\",\"1603117539111.jpeg\",\"1603117539116.jpeg\",\"1603117539141.jpeg\",\"1603117539188.jpeg\"]',1,NULL,'2020-10-19 14:25:39','2020-10-19 14:25:39',0),(12,'car',50000,0,'MERCEDES-BENZ S-KLASSE','null','[\"1603118080370.jpeg\",\"1603118080374.jpeg\",\"1603118080375.jpeg\",\"1603118080375.jpeg\",\"1603118080384.jpeg\"]',1,NULL,'2020-10-19 14:34:40','2020-10-19 14:34:40',0),(13,'house',100000,0,'Traumhaus3','null','[\"1603118274118.jpeg\"]',1,NULL,'2020-10-19 14:37:54','2020-10-19 14:37:54',0),(14,'house',500000,0,'Traumhaus4','null','[\"1603118305360.jpeg\"]',1,NULL,'2020-10-19 14:38:25','2020-10-19 14:38:25',0),(15,'travel',5000,0,'HOTEL','null','[\"1603118843701.jpeg\",\"1603118843712.jpeg\",\"1603118843725.jpeg\"]',1,NULL,'2020-10-19 14:47:23','2020-12-08 19:13:07',0),(16,'electronic',3000,0,'Ausrüstung','1\r\n2\r\n3\r\n4\r\n5\r\n6\r\n7\r\n8\r\n9\r\n10\r\n','[\"1604068696600.jpeg\",\"1604068592481.jpeg\"]',1,NULL,'2020-10-30 14:36:32','2020-10-30 14:38:16',0),(17,'house',526262,0,'Test','G','[\"1607014770223.jpeg\"]',0,NULL,'2020-12-03 16:59:30','2020-12-03 17:00:26',0),(18,'car',1224,0,'Test','Auto Lamm o jdjdndbssbj 27272882 \r\nJdjdjdbshjhh56777788 ueuueeuei\r\nHshgagshjdb45677372!\r\nGagshhdhdsb5678\r\nJdjhshs tb j jiji.   Nkkkkn ','[\"1607015481256.jpeg\"]',1,NULL,'2020-12-03 17:11:21','2020-12-03 17:11:21',0),(19,'car',111,0,'www','eeer','[\"1615390298254.jpg\"]',1,NULL,'2021-03-10 15:31:38','2021-03-10 15:31:38',0);
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `isAdmin` tinyint(1) DEFAULT '0',
  `ownLose` int(11) DEFAULT '0',
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'superadmin','test@admin.com','$2b$10$DxtKdH9bo9a1vIyOTg8Z0uAZqUceyotdPLHeyYQBh0V4qJfjunuv.',1,0,'2020-10-02 18:57:32','2020-10-02 18:57:32'),(3,'Chris Account 1','cm@webgondel.de','$2b$10$2U7GZ07CVGXcDFRfHP8vJ.h2hMw2ctlt1xpKlXuhUyFJbNBmb6VPm',0,0,'2020-10-06 15:16:45','2021-04-06 14:26:14'),(5,'nbg1212','info@webgondel-recruiting.de','$2b$10$qA9M.zqy/YUhjAcBvgf7ROAsk5gWYgAzrrvOZvkCbLeBVRwMGKTr6',0,0,'2020-10-07 09:59:01','2020-10-07 09:59:01'),(6,'Winner01','manuelconil89@gmail.com','$2b$10$OV5KMmBq6yrrNpRC0vIz3.yqp3yvY1iO6SVxCccF.1Zq5BRP/hfUa',0,0,'2020-10-09 10:38:20','2020-12-08 19:08:37'),(7,'ItaloConil88','lufinomanu@gmail.com','$2b$10$P2BN9WRnf8lXLdfWXy1Ds.I45KpCqcnT8H4M0xs27CoAiS7hzccaa',0,0,'2020-10-09 10:39:18','2020-10-09 10:39:18'),(16,'C.M.','christophmaximowitz@gmail.com','$2b$10$BxePg0ezKBSlaxxL2pB1b.ZNVcFnd5EvTKWxjfkaS46VOf1hBZBHi',0,0,'2020-10-12 06:09:44','2020-10-12 06:09:44'),(17,'LufinoManu','manuelelufino@gmail.com','$2b$10$5F/oeLEJAQ0YrNXBzPjqUuNpwPDVnvRPivYR9aoLqWk15nbLc/n0O',0,0,'2020-10-19 15:17:44','2020-10-19 15:17:44'),(18,'MaScher','s.manuela1@web.de','$2b$10$gEHFmFM.ktkNlraBRT.OHetXU5UeoN4J7ue1kVz4CV59CxAMKeFQ2',0,0,'2021-01-22 16:06:37','2021-01-22 16:06:37'),(19,'super','supermanit0517@outlook.com','$2b$10$n7btk0quxIvPWfkZPs2TdeJsvk4OVRHMny/OyfqYLFRyS5vbuQ0Cq',0,0,'2021-01-27 03:18:20','2021-01-27 03:18:20'),(20,'rosi','rosilufino84@gmail.com','$2b$10$1VfnpYMNmAd9zhNkDjQIceAMA9qDmjt9.aQwZUq4TaWEJyKrKc3t.',0,0,'2021-01-28 15:02:35','2021-01-28 15:02:35'),(21,'webgondel','rf@webgondel.de','$2b$10$UfTPkWef18B7cCekyNHJ5eiAvyrt6zqJDhOiSe4LYlrNXo1/PvH32',0,0,'2021-04-06 14:26:46','2021-04-06 14:26:46');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-22 15:47:53
