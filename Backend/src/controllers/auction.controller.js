import db from '../models';

const Auction = db.auction;
const Item = db.items;
const User = db.user;
const sequelize = db.sequelize;

exports.addAuction = async (data) => {
    const { item_id } = data;
    await Auction.create(data)
    const item = await Item.findOne({ where: { id: item_id }, attributes: ['total_quantity', 'additional_quantity'] });
    const total = parseInt(await Auction.sum('quantity', { where: { item_id } }), 10) + item.additional_quantity;

    if (total >= item.total_quantity) {
        Item.update({ isComplete: true }, { where: { id: item_id } });
        const data = await Auction.findAll(
            {
                raw: true,
                where: { item_id: item_id },
                group: ['user_id'],
                attributes: { include: [[db.sequelize.fn('sum', db.sequelize.col('quantity')), 'total_quantity']] },
            },
        );
        const randomNumber = Math.floor((Math.random() * (item.total_quantity - item.additional_quantity) + 1));
        let tempValue = 0;
        for (let i = 0; i < data.length; i++) {
            const total_quantity = parseInt(data[i].total_quantity, 10);
            if (randomNumber > tempValue && randomNumber <= tempValue + total_quantity) {
                await Item.update({ winner: data[i].user_id }, { where: { id: item_id } });
                break;
            } else {
                tempValue += total_quantity;
            }
        }
    }
    User.hasMany(Item, { foreignKey: 'winner' });
    Item.belongsTo(User, { foreignKey: 'winner' });
    return await Item.findOne({ where: { id: item_id }, include: [{ model: User }], });
};

exports.buyLose = async (req, res) => {
    const user = await User.findOne({ where: { id: req.body.user_id } });
    User.update({ ownLose: user.ownLose + req.body.quantity }, { where: { id: req.body.user_id } })
    res.status(200).send();
}

exports.getMyOwnLose = async (req, res) => {
    Item.hasMany(Auction, { foreignKey: 'item_id' });
    Auction.belongsTo(Item, { foreignKey: 'item_id' });
    const result = await Auction.findAll({
        where: { user_id: req.params.id },
        raw: true,
        include: [{ model: Item, where: { active: true } }],
        attributes: { include: [[db.sequelize.fn('sum', db.sequelize.col('quantity')), 'total_quantity']] },
    });
    res.status(200).send({ data: result[0].total_quantity });
}

exports.getTotalQuantity = async (req, res) => {
    let total = await Auction.sum('quantity', { where: { item_id: req.params.item_id } });
    if (!total) {
        total = 0;
    }
    res.status(200).send({ total });
};

exports.getAllBids = async (req, res) => {
    Item.hasMany(Auction, { foreignKey: 'item_id' });
    Auction.belongsTo(Item, { foreignKey: 'item_id' });
    const result = await Auction.findAll({
        where: { user_id: req.params.user_id },
        attributes: { include: [[db.sequelize.fn('sum', db.sequelize.col('quantity')), 'total_quantity']] },
        group: ['item_id'],
        include: {
            model: Item,
            where: {
                active: true
            }
        }
    });
    res.status(200).send(result)
}

exports.getAllBidsByItem = async (req, res) => {
    const allBid = await sequelize.query(`
        SELECT auctions.id, auctions.quantity, users.username, users.email, auctions.createdAt 
        FROM auctions 
        JOIN users ON auctions.user_id=users.id 
        WHERE auctions.item_id=${req.params.item_id}`, { type: db.Sequelize.QueryTypes.SELECT }
    );
    res.status(200).json(allBid);
}
