import db from '../models';

const Favourite = db.favourite;
const Items = db.items;
const Auction = db.auction;
const Op = db.Sequelize.Op;

exports.getFavourite = async (req, res) => {
    const result = await Favourite.findOne({where: {user_id: req.params.user_id, item_id: req.params.item_id}});
    res.status(200).send(result)
};

exports.getAllFavourite = async (req, res) => {
    Items.hasMany(Favourite, {foreignKey: 'item_id'});
    Favourite.belongsTo(Items, {foreignKey: 'item_id'});
    const result = await Favourite.findAll({
        where: {user_id: req.params.user_id},
        include: {
            model: Items,
            where: {
                active: true
            }
        }
    });
    let response = [];
    for (let i = 0; i < result.length; i++) {
        const item = {};
        item.item = result[i];
        item.sum = await Auction.sum('quantity', {where: {item_id: result[i].item_id}});
        response.push(item);
    }
    res.status(200).send(response);
};

exports.removeFavourite = async (req, res) => {
    const result = await Favourite.destroy({
        where: {
            user_id: req.params.user_id,
            item_id: req.params.item_id
        }
    });
    res.send(204).send();
};

exports.setFavourite = async (req, res) => {
    const result = await Favourite.create(req.body);
    res.send(result);
};
