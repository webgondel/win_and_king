import * as AuctionController from '../controllers/auction.controller';
import Paypal from '../helper/paypal';

const handlePaypal = new Paypal();

export default {
    // Pay ticket by checking order id
    async payTicket(req, res) {
        const { orderId, itemId, quantity } = req.body
        let content, item;
        try {
            const response = await handlePaypal.exec(orderId)
            if (response.result.status === 'COMPLETED') {
                // Pay ticket                
                const result = await AuctionController.addAuction({
                    user_id: req.user.id,
                    item_id: itemId,
                    quantity
                })
                content = 'Your payment was successful!'
                item = result
            }
        } catch (e) {
            console.log('error', e);
        }
        return res.send({ content, item })
    }
}