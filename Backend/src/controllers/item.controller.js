// import * as fs from 'fs';
import db from '../models';

const Items = db.items;
const User = db.user;
// const Favourite = db.favourite;
// const Op = db.Sequelize.Op;

exports.createItem = (req, res) => {
    let picture = [];
    for (let file of req.files) {
        picture.push(file.filename)
    }

    const newItem = {
        type: req.body.type,
        total_quantity: req.body.total_quantity,
        isComplete: 0,
        name: req.body.name,
        description: req.body.description,
        preview_images: JSON.stringify(picture)
    };

    Items.create(newItem).then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || 'Error occurred while processing'
        })
    });
};

exports.getItems = async (req, res) => {
    User.hasMany(Items, {foreignKey: 'winner'});
    Items.belongsTo(User, {foreignKey: 'winner'});
    const items = await Items.findAll({
        where: {active: true},
        include: [{model: User}],
    });
    res.status(200).send(items)
};

exports.updateItem = async (req, res) => {
    let picture = [];
    for (let file of req.files) {
        picture.push(file.filename)
    }
    const id = req.params.id;
    // const data = req.body;
    const updateData = {
        type: req.body.type,
        total_quantity: req.body.total_quantity,
        isComplete: 0,
        additional_quantity: req.body.percentage,
        name: req.body.name,
        description: req.body.description,
        preview_images: JSON.stringify([...picture, ...JSON.parse(req.body.old_images)])
    };
    await Items.update(updateData, {where: {id}});
    const updatedItem = await Items.findByPk(id);
    res.status(200).json(updatedItem);
}

exports.deleteItem = async (req, res) => {
    const id = req.params.id;
    // const item = await Items.findByPk(id);
    // await Items.destroy({where: {id}});
    // item.preview_images = JSON.parse(item.preview_images);
    // for (const image of item.preview_images) {
    //     fs.unlink('./public/' + image, function (err) {
    //     });
    // }
    // Favourite.destroy({where: {item_id: id}});
    await Items.update({active: false}, {where: {id}});
    // fs.unlink()
    res.status(204).json({});
}