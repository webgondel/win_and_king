import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import ejs from 'ejs';

import db from '../models';
import config from "../config/config";
import {sendEmail} from '../helper/sendEmail';

const User = db.user;
const Op = db.Sequelize.Op;
const saltRounds = 10;

exports.register = async (req, res) => {
    const newUser = req.body;
    bcrypt.hash(newUser.password, saltRounds, function (err, hash) {
        newUser.password = hash;
        User.create(newUser).then( async data => {
            const content = await ejs.renderFile('src/template/register_success.html', {
                title: 'WELCOME TO WIN & KING!',
                content: 'YOUR REGISTRATION WAS SUCCESSFUL!'
            });
            const msg = {
                to: newUser.email,
                from: process.env.OWNER_EMAIL,
                subject: 'Register Success!',
                html: content
            };
            sendEmail(msg);
            res.send(data);
        }).catch(err => {
            console.log(err)
            res.status(500).send({
                message: err.message || "Some error occurred while registering"
            });
        });
    })
};

exports.login = async (req, res) => {
    const auth_data = req.body;
    const user = await User.findOne({where: {email: auth_data.email.toLowerCase()}}).then(res => {
        return res;
    });
    if (user) {
        const passwordIsValid = bcrypt.compareSync(auth_data.password, user.password);
        if (passwordIsValid) {
            // Passwords match
            const token = jwt.sign({
                id: user.id,
                email: user.email,
                username: user.username,
                isAdmin: user.isAdmin
            }, config.jwtSecret, {
                expiresIn: 86400 * 30    //expires in 24 hours
            });
            res.json({token});
        } else {
            // Passwords don't match
            res.status(400).send({
                message: 'Incorrect Email Or Password!'
            });
        }
    } else {
        res.status(400).send({
            message: 'Incorrect Email Or password!'
        });
    }
};

exports.updateUserInfo = async (req, res) => {
    const user_info = req.body;
    await User.update(
        {email: user_info.email, username: user_info.username},
        {where: {id: user_info.id}}
    ).then(res => {
        return res;
    });
    const user = await User.findOne({where: {id: user_info.id}}).then(res => {
        return res;
    });

    if (user) {
        const token = jwt.sign({
            id: user.id,
            email: user.email,
            username: user.username,
            isAdmin: user.isAdmin
        }, config.jwtSecret, {
            expiresIn: 86400    //expires in 24 hours
        });
        res.json({token});
    } else {
        res.status(400).send({
            message: 'Update Failed'
        });
    }
};

exports.updatePassword = async (req, res) => {
    const password = req.body;
    const user = await User.findOne({where: {id: password.id}}).then(res => {
        return res;
    });

    if (user) {
        const passwordIsValid = bcrypt.compareSync(password.oldPassword, user.password);
        if (passwordIsValid) {
            bcrypt.hash(password.newPassword, saltRounds, function (err, hash) {
                User.update(
                    {password: hash},
                    {where: {id: password.id}}
                ).then(res => {
                });
                res.json({message: 'success'});
            })
        } else {
            // Passwords don't match
            res.status(400).send({
                message: 'failed'
            });
        }
    } else {
        res.status(400).send({
            message: 'failed'
        });
    }
}

exports.resetPassword = async (req, res) => {
    const resetPassword = generateKey(6);
    const hash = bcrypt.hashSync(resetPassword, saltRounds);
    const updated = await User.update({password: hash}, {where: {email: req.body.email}});
    if (updated[0] === 0) {
        res.status(400).json({message: 'Email not exists'});
        return;
    }

    // sgMail.setApiKey(config.SENDGRID_API_KEY);
    const msg = {
        to: req.body.email,
        from: process.env.OWNER_EMAIL,
        subject: 'Reset Password',
        html: 'Your password was reset into <strong>' + resetPassword + '</strong>'
    };
    // await sgMail.send(msg);
    sendEmail(msg)

    res.status(200).json({message: 'Your password was reset'})
};

function generateKey(length) {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

