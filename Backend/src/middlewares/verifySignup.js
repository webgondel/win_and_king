import db from '../models';

const User = db.user;

const checkDuplicateUserEmailOrEmail = (req, res, next) => {
    // Check Username is already in use
    User.findOne({
        where: {
            username: req.body.username
        }
    }).then(user => {
        if (user) {
            res.status(400).json({message: "Username is already taken!"});
            return;
        }

        //Check Email is already in use

        User.findOne({
            where: {
                email: req.body.email
            }
        }).then(user => {
            if (user) {
                res.status(400).json({message: "Email is already in use"});
                return;
            }

            next();
        })

    })
};

const signUpVerify = {};

signUpVerify.checkDuplicateUserNameOrEmail = checkDuplicateUserEmailOrEmail;

export default signUpVerify;
