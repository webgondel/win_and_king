import { config as envConfig } from 'dotenv';

envConfig();
// Default config
const config = {
    env: process.env.NODE_ENV,
    port: process.env.PORT,
    jwtSecret: process.env.JWT_SECRET,
    dialect: process.env.DIALECT,
    mysql: {
        host: process.env.MYSQL_HOST,
        user: process.env.MYSQL_USER,
        password: process.env.MYSQL_PASSWORD,
        database: process.env.MYSQL_DATABASE,
    },
    SENDGRID_API_KEY: process.env.SENDGRID_API_KEY
};

export default config;
