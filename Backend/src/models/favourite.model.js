module.exports = (sequelize, Sequelize) => {
    return sequelize.define('favourite', {
        user_id: {
            type: Sequelize.INTEGER,
        },
        item_id: {
            type: Sequelize.INTEGER
        },
    });
};
