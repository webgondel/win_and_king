module.exports = (sequelize, Sequelize) => {
    return sequelize.define('items', {
        type: {
            type: Sequelize.ENUM,
            values: ['car', 'house', 'travel', 'electronic']
        },
        total_quantity: {
            type: Sequelize.INTEGER
        },
        additional_quantity: {
            type: Sequelize.INTEGER, defaultValue: 0, allowNull: false
        },
        isComplete: {
            type: Sequelize.BOOLEAN
        },
        name: {
            type: Sequelize.STRING
        },
        description: {
            type: Sequelize.STRING
        },
        preview_images: {
            type: Sequelize.STRING
        },
        active: {
            type: Sequelize.BOOLEAN,
            defaultValue: true,
            allowNull: false,
        },
        winner: {
            type: Sequelize.INTEGER
        }
    });
};
