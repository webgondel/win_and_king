module.exports = (sequelize, Sequelize) => {
    return sequelize.define('auctions', {
        user_id: {
            type: Sequelize.INTEGER,
        },
        item_id: {
            type: Sequelize.INTEGER
        },
        quantity: {
            type: Sequelize.INTEGER
        }
    });
};
