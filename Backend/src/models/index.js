import config from '../config/config';
import Sequelize from "sequelize";
import user from './user.model';
import items from './item.model';
import favourite from './favourite.model';
import auction from './auction.model';

const sequelize = new Sequelize(config.mysql.database, config.mysql.user, config.mysql.password, {
    host: config.mysql.host,
    dialect: config.dialect,
    operatorsAliases: '0',
    logging: false
});

const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.user = user(sequelize, Sequelize);
db.items = items(sequelize, Sequelize);
db.favourite = favourite(sequelize, Sequelize);
db.auction = auction(sequelize, Sequelize);

export default db;
