import {Router} from 'express';
import * as FavouriteController from '../controllers/favourite.controller';

const router = Router();

router.route('/get_favourite/:user_id/:item_id').get(FavouriteController.getFavourite);
router.route('/get_all_favourites/:user_id').get(FavouriteController.getAllFavourite);
router.route('/remove_favourite/:user_id/:item_id').delete(FavouriteController.removeFavourite);
router.route('/set_favourite').post(FavouriteController.setFavourite);

export default router;
