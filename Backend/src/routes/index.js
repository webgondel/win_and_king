import { Router } from 'express';
import testRouter from './test.router';
import authRouter from './auth.router';
import itemRouter from './item.router';
import paymentRouter from "./payment.router"
import favouriteRouter from './favourite.router';
import auctionRouter from './auction.router';

const router = Router();

router.use('/test', testRouter);
router.use('/auth', authRouter);
router.use('/item', itemRouter);
router.use('/favourite', favouriteRouter);
router.use('/auction', auctionRouter);
router.use('/payment', paymentRouter);

export default router;
