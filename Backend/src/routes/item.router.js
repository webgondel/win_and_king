import multer from "multer";
import {Router} from 'express';
import * as ItemController from '../controllers/item.controller';

function getExt(filename) {
    const ext = filename.split('.').pop();
    if(ext === filename) return "";
    return ext;
}

const uploadItemFile = multer({
    storage: multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, './public');
        },
        filename: function (req, file, cb) {
            cb(null, Date.now() + '.' + (getExt(file.originalname) ? getExt(file.originalname) : 'png'));
        }
    }),
    limits: {fileSize: 20000000}
});

const router = Router();

router.route('/create_item').post(uploadItemFile.array('preview_images', 20), ItemController.createItem);
router.route('/get_items').get(ItemController.getItems);
router.route('/update_item/:id').put(uploadItemFile.array('preview_images', 20), ItemController.updateItem);
router.route('/delete_item/:id').delete(ItemController.deleteItem);

export default router;
