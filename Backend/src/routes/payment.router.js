import { Router } from 'express';
import paymentController from '../controllers/payment.controller';

const router = Router();

router.route('/pay-ticket').post(paymentController.payTicket);

export default router;
