import {Router} from 'express';
import * as AuctionController from '../controllers/auction.controller';

const router = Router();

router.route('/buy_lose').post(AuctionController.buyLose);
router.route('/getmyownlose/:id').get(AuctionController.getMyOwnLose);
router.route('/get_total_quantity/:item_id').get(AuctionController.getTotalQuantity);
router.route('/get_all_bids/:user_id').get(AuctionController.getAllBids);
router.route('/getPurchaseHistory/:item_id').get(AuctionController.getAllBidsByItem)

export default router;
