import {Router} from 'express';
import * as userController from '../controllers/auth.controller';
import verifySignup from '../middlewares/verifySignup'

const router = Router();

router.route('/register').post([verifySignup.checkDuplicateUserNameOrEmail], userController.register);
router.route('/login').post(userController.login);
router.route('/updateUserInfo').put(userController.updateUserInfo);
router.route('/updatePassword').put(userController.updatePassword);
router.route('/resetPassword').put(userController.resetPassword);
export default router;
