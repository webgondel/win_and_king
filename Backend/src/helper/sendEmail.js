import sgMail from '@sendgrid/mail';
import config from "../config/config";

export const sendEmail = (data) => {
    sgMail.setApiKey(config.SENDGRID_API_KEY);
    sgMail.send(data);
}