import paypal from '@paypal/checkout-server-sdk'

class Paypal {
    orderID = null;
    // Setup paypal gateway 
    constructor() {
        const mode = process.env.PAYPAL_MODE
        const client_id = process.env.PAYPAL_ID
        const client_secret = process.env.PAYPAL_SECRET
        let environment = mode == 'sandbox' ?
            new paypal.core.SandboxEnvironment(client_id, client_secret)
            : new paypal.core.LiveEnvironment(client_id, client_secret)

        this.client = new paypal.core.PayPalHttpClient(environment);
    }
    // Create a link for payment procced 
    async createLink(req, { price, quantity }) {
        const link = req.protocol + '://' + req.get('host');
        let request = new paypal.orders.OrdersCreateRequest();
        request.requestBody({
            "payment_source_response": "card",
            "application_context": {
                "return_url": `${link}/api/payment/pay-ticket`,
                "cancel_url": `${link}/api/payment/pay-ticket?cancel=true`,
                "brand_name": "Win and King",
            },
            "intent": "CAPTURE",
            "purchase_units": [
                {
                    "amount": {
                        "currency_code": "EUR",
                        "value": Number(+price * quantity).toFixed(2)
                    }
                }
            ]
        });
        let response = await this.client.execute(request);
        this.orderID = response.result.id;
        return {
            id: response.result.id,
            link: response.result.links.find((l) => l.rel == 'approve').href
        }
    }
    // Execute Order
    async exec(token) {
        const request = new paypal.orders.OrdersCaptureRequest(token);
        request.requestBody({});
        // Call API with your client and get a response for your call
        let response = await this.client.execute(request);
        return response
    }
}

export default Paypal